import { define } from '@xinix/xin';
import { View } from '@xinix/xin/views';

import html from './my-book.html';
// import './my-.scss';

import('xin-ui/ui-slides');

export class MyBook extends View {
  get props () {
    return Object.assign({}, super.props, {
      title: {
        type: String,
        value: 'Books',
      },
      lists: {
        type: Array,
        value: () => ([]),
      },
    });
  }

  get template () {
    return html;
  }

  focused() {
    // let result = await fetchService.find('myListApp');
    // this.set('lists', result);
    const list = [{ id: 1, title: 'Harry Potter', author: 'Jk rowling', stock: '2' }, { id: 2, title: 'Harry Potter 2', author: 'Jk rowling', stock: '13' }, { id: 3, title: 'Harry Potter 3', author: 'Jk rowling', stock: '1' }];
    this.set('lists', list);
  }

  doLogin (evt) {
    evt.preventDefault();

    this.__app.navigate('/');
  }

  edit(book) {
    this.__app.navigate(`/books/${book.id}`);
  }
}
define('my-book', MyBook);
