import { define } from '@xinix/xin';
import { View } from '@xinix/xin/views';

import html from './my-profile.html';
import './my-profile.scss';

import('xin-ui/ui-slides');
import('xin-ui/ui-textfield');

export class MyProfile extends View {
  get props () {
    return Object.assign({}, super.props, {
      title: {
        type: String,
        value: 'Profile',
      },

      profile: {
        type: Object,
        value: () => ({
          name: 'Michael Jordan',
          birthdate: '1982-11-21',
          image: '../assets/images/profile/200x200jordan.png',
        }),
      },
      list: {
        type: Array,
        value: () => ([]),
      },
    });
  }

  get template () {
    return html;
  }

  focused () {
    // let result = await fetchService.find('myListApp');
    // this.set('lists', result);
    const list = [{ id: 1, title: 'Harry Potter', author: 'Jk rowling', endDate: '28 okt 2018' }, { id: 2, title: 'Harry Potter 2', author: 'Jk rowling', endDate: '21 Jan 2018' }, { id: 3, title: 'Harry Potter 3', author: 'Jk rowling', endDate: '28 Jan 2018' }];
    this.set('lists', list);
  }

  async doSave (evt) {
    evt.preventDefault();

    let { UISnackbar } = await import('xin-ui/ui-snackbar');
    await UISnackbar.show({ message: 'Profile saved' });
  }
}
define('my-profile', MyProfile);
