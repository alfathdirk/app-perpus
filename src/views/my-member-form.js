import { define } from '@xinix/xin';
import { View } from '@xinix/xin/views';

import html from './my-member-form.html';

import('xin-ui/ui-reveal');
import('xin-ui/ui-textfield');

export class MyMemberForm extends View {
  get props () {
    return Object.assign({}, super.props, {
      title: {
        type: String,
        value: 'Form',
      },
    });
  }

  get template () {
    return html;
  }

}
define('my-member-form', MyMemberForm);
