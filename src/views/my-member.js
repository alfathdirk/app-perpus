import { define } from '@xinix/xin';
import { View } from '@xinix/xin/views';

import html from './my-member.html';
// import './my-.scss';

import('xin-ui/ui-slides');

export class MyMember extends View {
  get props () {
    return Object.assign({}, super.props, {
      title: {
        type: String,
        value: 'All Member',
      },
      lists: {
        type: Array,
        value: () => ([]),
      },
    });
  }

  get template () {
    return html;
  }

  focused() {
    // let result = await fetchService.find('myListApp');
    // this.set('lists', result);
    const list = [{ name: 'Dimas', role: 'Admin' }, { name: 'Dinda', role: 'Member' }, { name: 'Danang', role: 'Member' }];
    this.set('lists', list);
  }

  doLogin (evt) {
    evt.preventDefault();

    this.__app.navigate('/');
  }

  edit(book) {
    this.__app.navigate(`/member/${book.name}`);
  }
}
define('my-member', MyMember);
